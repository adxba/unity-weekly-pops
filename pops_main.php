<?php

/*$zipName = "PRI1.zip";
ob_start();
header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename='.basename($zipName));
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');*/

require_once('mailer/PHPMailerAutoload.php');
require_once('includes/sqlconnect.php');

$popUploadDirectory = "../../uploads/pops/";
$zipParentDirectory = "zips/";
$dateNow = date("Y-m-d");

$zipNowDirectory = $zipParentDirectory.$dateNow."/";

@mkdir($zipNowDirectory, 0777, true);

$query = "SELECT `ticket_code`,`from`,`to`,`cc`,`subject`,`message`, `start_date`, `end_date` 
				FROM weekly_pops
				;";
$result = $unity_connection->query($query);

if(mysqli_num_rows($result) > 0)
{
	while($row = mysqli_fetch_array($result))
	{
		$ticket_code = $row["ticket_code"];
		$from = $row["from"];
		$to = $row["to"];
		$cc = $row["cc"];
		$subject = $row["subject"];
		$message = $row["message"];
		$start_date = $row["start_date"];
		$end_date = $row["end_date"];

		$dateTimeNow = date('Y-m-d H:i:s');
		$start_date = date('Y-m-d H:i:s',strtotime($start_date));
		$end_date = date('Y-m-d H:i:s',strtotime($end_date));
		$end_dateOnly = date('Y-m-d',strtotime($end_date));

		//Get Locations for Ticket Code. e.g PRI -> Primesight units.
		$queryLocations = "SELECT locations 
							FROM user_groups 
							WHERE ticket_code = '".$ticket_code."' LIMIT 1
							;";
		$resultLocations = $unity_connection->query($queryLocations);

		if(mysqli_num_rows($resultLocations) > 0)
		{
			while($rowLocations = mysqli_fetch_array($resultLocations))
			{
				$locations = $rowLocations["locations"];

				$locationsArray = array();
				if($locations == "-1")
				{
					$locationsArray = GetAllLocationsArray();
				}
				else
				{
					$locationsArray = explode(',', $locations);
				}

				$queryReports = "SELECT m.unit_id, m.pop_urls, m.`date`, u.name 
									FROM maintenance_reports m 
									LEFT JOIN units u 
									ON m.unit_id = u.id 
									LEFT JOIN locations l
									ON u.location_id = l.id
									";

				$locWhereQuery = "";
				$counter = 0;
				foreach($locationsArray as $key => $value)
				{
					if($counter == 0)
					{
						$locWhereQuery = " WHERE (u.location_id = ".$value;
					}
					else
					{
						$locWhereQuery .= " OR u.location_id = ".$value;
					}
					$counter ++;
				}
				if($counter > 0)
				{
					$locWhereQuery .= ")";
				}
				$queryReports .= $locWhereQuery;
				$queryReports .= " AND (m.`date` BETWEEN '".$start_date."' AND '".$end_date."')
				;";

				$resultReports = $unity_connection->query($queryReports);

				if(mysqli_num_rows($resultReports) > 0)
				{
					$popurls = "";
					$poparray = array();
					//$popAfterarray = array();
					while($rowReports = mysqli_fetch_array($resultReports))
					{
						$popurls = $rowReports["pop_urls"];
						$datePOP = $rowReports["date"];
						$unitName = $rowReports["name"];
						
						$zipName = $zipNowDirectory.$ticket_code."-".$dateNow.".zip";
						ZipFiles($popurls, $popUploadDirectory, $zipName, $datePOP, $unitName);
					}
					sendMailWithAttachment($zipName, $from, $to, $cc, $subject, $message);
				}
			}
		}

		$dateTimeNowTimestamp = strtotime($dateNow);
		$enddateTimeStamp = strtotime($end_dateOnly);
		if($dateTimeNowTimestamp >= $enddateTimeStamp)
		{
			ResetStartEndDate($start_date, $end_date, $ticket_code);
		}
	}

}

?>

<?php

function GetAllLocationsArray()
{
	$locations = array();

	global $unity_connection;

	$query = "SELECT id from locations;";
	$resultLocations = $unity_connection->query($query);

	if(mysqli_num_rows($resultLocations) > 0)
	{
		while($row = mysqli_fetch_array($resultLocations))
		{
			$location = $row["id"];
			array_push($locations, $location);
		}
	}
	return $locations;
}

function AddPOPsToArray($poparray, $popurls)
{
	$popurlsJ = json_decode($popurls, true);

	foreach ($popurlsJ as $key => $value)
	{
		//if($key == $type)
		//{
			foreach ($value as $k => $v)
			{
				$filename = $v;
				array_push($poparray, $filename);
			}
		//}
	}

	return $poparray;
}

function ZipFiles($popurls, $popUploadDirectory, $zipName, $datePOP, $unitName)
{
	//echo "popurls zip: ".$popurls."<br/>";
	//$files = array("2.jpg", "1.jpg");
	//$zipName = $file;
	$zip = new ZipArchive;
	$zip->open($zipName, ZipArchive::CREATE);
	
	$extension = "";


	$popurlsJ = json_decode($popurls, true);

	foreach ($popurlsJ as $key => $value)
	{
		//$key = Before / After.

		foreach($value as $index => $filename)
		{
			$unitAndDate = explode('_', $filename);
			if(count($unitAndDate) > 1)
			{
				$dateAndTimestamp = explode('-', $unitAndDate[1]);
				if(count($dateAndTimestamp) > 0)
				{
					$date1 = date('Y-m-d', strtotime($datePOP));
					$time1 = date('H:i', strtotime($datePOP));
					$time1 = str_replace(":", "", $time1);
					$filepath = $date1."/".$unitName."-".$time1."/".$key."/".$filename;
					//echo "filepath: ".$filepath."<br/>";

					$zip->addFile($popUploadDirectory.$filename, $filepath);
				}
			}
		}
	}
	$zip->close();
}

function DownloadFile($file) { // $file = include path
    if(file_exists($file)) {
    	header('Content-Length: ' . filesize($file));
    	ob_clean();
    	ob_flush();
        readfile($file);
        exit;
    }
}

function sendMailWithAttachment($filename, $from, $to, $cc, $subject, $message)
{
	global $unity_connection;

	$zipFilePath = "https://api.unityplatform.com/cron/weekly-pops/".$filename;

	//Create a new PHPMailer instance
	$mail = new PHPMailer;

	$mail->Subject = $subject;

	$bodyHTML = "Hi,";
	$bodyHTML .= "<p>Please view the Weekly POPs by accessing the zip file below: </p>";
	$bodyHTML .= "<p>".$zipFilePath."</p>";
	$bodyHTML .= "<p>Thanks</p>";
	$mail->msgHTML($bodyHTML);

	$contactsFromNameArray = explode(";",$from);
	$contactsToArray = explode(",",$to);
	$contactsCCArray = explode(",",$cc);

	//Set From.
	//echo "From = ".$contactsFromNameArray[0]."<br/>";
	$mail->setFrom($contactsFromNameArray[0],$contactsFromNameArray[1]);
	
	//Set To.
	for($i=0;$i<count($contactsToArray);$i++) 
	{
		$contactsToNameArray = explode(";",$contactsToArray[$i]); // Email | Name
		$mail->addAddress($contactsToNameArray[0], $contactsToNameArray[1]);
		//echo "To = ".$contactsToNameArray[0]."<br/>";
	}

	if($cc != "")
	{
		for($i=0;$i<count($contactsCCArray);$i++) 
		{
			$contactsCCNameArray = explode(";",$contactsCCArray[$i]); // Email | Name
			$mail->addCC($contactsCCNameArray[0], $contactsCCNameArray[1]);
			//echo "CC = ".$contactsCCNameArray[0]."<br/>";
		}
	}
	//echo "filename = ".$filename."<br/>";

	//$mail->addAttachment("test.pzip");

	if (!$mail->send()) 
	{
    	echo "Mailer Error: " . $mail->ErrorInfo;
	}
	else 
	{
  		echo "Message sent, To: ".$to.", ";
  		echo "Message sent, CC: ".$cc;
  		echo "<br/>";
	}
}

function ResetStartEndDate($startDate, $endDate, $ticket_code)
{
	$startTimestamp = strtotime($startDate);
	$endTimestamp = strtotime($endDate);

	$dateDiff = $endTimestamp - $startTimestamp;
	$daysDiff = floor($dateDiff/(60*60*24));

	$start_date_new = date('Y-m-d H:i:s', strtotime($startDate. '+ '.$daysDiff.' days'));
	$end_date_new = date('Y-m-d H:i:s', strtotime($endDate. '+ '.$daysDiff.' days'));

	global $unity_connection;

	$query = "UPDATE weekly_pops SET start_date = '".$start_date_new."', end_date = '".$end_date_new."' 
				WHERE ticket_code = '".$ticket_code."'
				;";

	$result = $unity_connection->query($query);
}

?>