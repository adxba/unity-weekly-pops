<?php
	class SQLConnection {
		private $mysqli;
		
		public function __construct ($host, $user, $pass, $name) {
			//Connect
			$this->mysqli = new mysqli($host, $user, $pass, $name);
		
			//If the DB is unreachable refresh in 5 seconds
			$connectionAttempts = 1;
			while ($this->mysqli == null && $connectionAttempts < 5){
				sleep(1);
				$this->mysqli = new mysqli($host, $user, $pass, $name);
				$connectionAttempts++;
			}
			
			// If no connection then close connection
			if($this->mysqli->connect_error){
				die("$this->mysqli->connect_errno: $this->mysqli->connect_error");
			}
		}
		
		public function __destruct(){
			$this->mysqli->close();
		}
		
		public function query($sql, $params = null){
			// prepare statement to prevent sql injection
			/*$stmt = $this->mysqli->prepare($sql);
			
			// loop through the params and bind to stmt if params is not null
			if ($params != null){
				foreach ($params as $param) {
					$value = key($param);
					$type = $param[$value];
					$stmt->bindParam($type, $param);
				}
			}
			
			// the result will be bind to $result
			$result = $stmt->get_result();
			// fetch the result
			$stmt->fetch();
			
			var_dump($this->mysqli);
			return $result;
			$stmt->close(); */
			
			// easy way
			$result = $this->mysqli->query($sql);
			return $result;
			
		}
		
		public function real_escape_string($string){
			return $this->mysqli->real_escape_string($string);
		}
		
		public function getConnection(){
			return $this->mysqli;
		}
		
		//getPreparedQuery('SELEC **8 ? ?', array(array($param1, 's'))
	}
?>